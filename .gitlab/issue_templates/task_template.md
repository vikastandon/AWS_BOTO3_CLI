## Outline

{__Outline__}

## Goal for this Task

- Create Outline Specification
- Reviewed by Tsujisan.
- Merged your Specification for this tool on Git Repogitory

## Expected  status of out puts

|No.|Output|Supplement|
|--|-----|---------|
|1|UseCase.pl|The use case for this tool with UML written by Plant UML.|
|2|Sequence Diagram|Sequence Diagram of this tools.|
|3|Document|Spec Document written by markdown|

## Git Project for this task

AllMembers/FditUtils

## Dirs for this task on git project

|dir|purpose|
|--|-----|
|TicketIssuer/doc/uml/ | for uml diagrams|
|TicketIssuer/doc/ |for spec documents|

## Git branch for work

dev_outlien_specification_for_ticketissuer_001

## Git branch for receive

rel_outlien_specification_for_ticketissuer_001

## Git source branch

(master)

## Merge Request for the task

WIP:Merge Request for "Outline Specification for TicketIssuer"

/label ~task 
/assign @vikastandon

## Review

After finishing your work you should be reviewd by Tsujisan for mearge
